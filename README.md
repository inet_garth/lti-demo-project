# LTI Demo Project #

This project was created using Visual Studio 2015, and tested on IIS 8.5

### What is this project? ###

* Demonstrate how to connect a web application to the Canvas platform using LTI
* This project was created as part of a series of posts in the Canvas Community:
    *  [Part 1 - Connect to Canvas](https://community.canvaslms.com/groups/canvas-developers/blog/2016/09/13/net-lti-project-part-1)
    *  [Part 2 - Launch Request](https://community.canvaslms.com/groups/canvas-developers/blog/2016/09/14/net-lti-project-part-2-launch-request)
    *  [Part 3 - OAuth](https://community.canvaslms.com/groups/canvas-developers/blog/2016/09/20/net-lti-project-part-3-oauth)

### IMPORTANT NOTE ###
This code is for demonstration purposes only, and should NOT be considered production ready.

The core code is based on a simple hello world tutorial found here:  

https://www.asp.net/mvc/overview/getting-started/introduction/getting-started


No real effort was put into creating functionality beyond what is discussed in the Community articles, and is not meant to be a fully developed solution.
The code in this project is meant only to clarify the Community articles.